#!/bin/bash

set -e

osver=$(grep VERSION_ID /etc/os-release | sed 's/VERSION_ID=//g' | tr -d '="')

mkdir -p /tmp/VNux/updatescript
cd /tmp/VNux/updatescript

if [[ $osver = "2022.6a" ]]; then
	echo "===>Không có gì để làm :V<==="
else
	echo "===>Vui lòng năng cấp lên bản 2022.6a qua iso<==="
fi
read -n 1 -s -r -p "===>Nhấn bất kì phím nào để thoát<==="
exit 0
